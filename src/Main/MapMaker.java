package Main;

import Items.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class MapMaker extends JPanel {

    private static MainMenu mainFrame;
    private Timer timer;
    private ArrayList<Block> blocks = new ArrayList<>();
    private int[][] map;

    public MapMaker(MainMenu mainFrame1) {
        mainFrame = mainFrame1;
        map = Map.getBlankMap();
        initBlocks();
        setFrameSize();
        setBackground(Color.BLACK);
        timer = new Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                initBlocks();
                repaint();
            }
        });
        timer.start();
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == 1) {
                    change(e.getX() / 16, e.getY() / 16);
                }
            }
        });
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                    change(e.getX() / 16, e.getY() / 16);
            }
        });
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == e.VK_ESCAPE) {
                    if (0 == JOptionPane.showConfirmDialog(null, "Do you want to save this as a new Stage ?", "Save?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE))
                        saveStage();
                    backToMenu();
                }
            }
        });
    }

    private void backToMenu() {
        Menu menu = new Menu(mainFrame);
        mainFrame.getGamePanel().removeAll();
        mainFrame.getGamePanel().add(menu);
        menu.revalidate();
        menu.repaint();
        mainFrame.setVisible(true);
        mainFrame.setSize(new Dimension(600, 600));
        mainFrame.setLocationRelativeTo(null);
        menu.requestFocusInWindow();
    }

    private void saveStage() {
        File stages = new File("stages/");
        File newStage = new File("stages/" + (stages.listFiles().length + 1) + ".JtankMap");
        PrintWriter out = null;
        try {
            out = new PrintWriter(newStage.getAbsoluteFile());
            String str = "";
            for (int i = 0; i < map.length; i++) {
                for (int j = 0; j < map[0].length; j++) {
                    if (map[i][j] == 5)
                        continue;
                    switch (map[i][j]) {
                        case 0:
                            str += ".";
                            break;
                        case 1:
                            str += "#";
                            break;
                        case 2:
                            str += "@";
                            break;
                        case 3:
                            str += "~";
                            break;
                        case 4:
                            str += "%";
                            break;
                    }
                }
                out.println(str);
                str = "";
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    private void change(int i, int j) {
        for (Block a : blocks) {
            if (a.x / 16 == i && a.y / 16 == j)
                if (a.getType() != 5) {
                    map[j][i] = (map[j][i] + 1) % 5;
                    return;
                }
        }
        map[j][i] = 1;
    }

    private void addBlock(int i, int j) {
        blocks.add(new Brick(i * 16, j * 16));
        map[i][j] = 1;
    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (Block a : blocks) {
            if (a.isVisible()) {
                g.drawImage(a.getImage(), a.getX(), a.getY(), this);
            }
        }
        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * Initialize blocks according to the map.
     */
    public void initBlocks() {
        int type;
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
                type = map[x][y];
                BlockType bType = BlockType.getTypeFromInt(type);
                switch (bType) {
                    case BRICK:
                        blocks.add(new Brick(y * 16, x * 16));
                        break;
                    case STEEL:
                        blocks.add(new Steel(y * 16, x * 16));
                        break;
                    case RIVER:
                        blocks.add(new River(y * 16, x * 16));
                        break;
                    case TREE:
                        blocks.add(new Tree(y * 16, x * 16));
                        break;
                    case EDGE:
                        blocks.add(new Edge(y * 16, x * 16));
                        break;
                    default:
                        break;
                }
            }
        }

    }

    private void setFrameSize() {
        Block lastB = blocks.get(blocks.size() - 1);
        mainFrame.setSize(new Dimension(lastB.x + lastB.width + 18, lastB.y + lastB.height + 47));
        mainFrame.setLocationRelativeTo(null);
    }

}
