package Main;

import Items.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class GameBoard extends JPanel implements ActionListener, Serializable {

    private static MainMenu mainFrame;
    private Timer timer;
    private Tank tank1;
    private Tank tank2;
    private Integer stage;
    private ArrayList<Item> allItems = new ArrayList<>();
    private ArrayList<Block> blocks = new ArrayList<>();
    private ArrayList<Animation> animations = new ArrayList<>();
    private ArrayList<TankAI> enemy = new ArrayList<>();
    private ArrayList<PowerUp> powerUps = new ArrayList<>();
    private ArrayList<Bullet> bullets = new ArrayList<>();
    private int hardness;
    private int goal;
    private int numEnemiesDestroyed;
    public int numEnemies;
    public static boolean gameOver, win, pause;

    private int yPos = Map.mapHeight;
    private int direction = -1;
    private final int stopYPos = 250;

    private Boolean singlePlayer;
    private Boolean isHost;
    private Server server;
    private Client client;

    private String cheatBuffer;

    public void loadGame(){
        CollisionUtility.loadCollisionUtility(this, blocks, animations);
        BoardUtility.loadBoardUtility(enemy, blocks, animations, tank1, powerUps);
        try {
            Map.getMap(stage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        addKeyListener(new KTAdapter());
        addMouseListener(new MTAdapter());
        addMouseMotionListener(new MTAdapter());
        tank1.makeGunFireTimer();
    }
    public static void setMainFrame(MainMenu mainFrame1) {
        mainFrame = mainFrame1;
    }

    public ArrayList<Item> getAllItems() {
        return allItems;
    }


    public Boolean isSinglePlayer() {
        return singlePlayer;
    }

    public GameBoard(MainMenu mainMenu1, int hardness, int stage, boolean singlePlayer, boolean isHost, Object socket) {
        this.singlePlayer = singlePlayer;
        if (this.singlePlayer) {
            gameOver = false;
            pause = false;
            win = false;
            cheatBuffer="";
            this.mainFrame = mainMenu1;
            mainFrame.setGame(this);
            this.stage = stage;
            this.hardness = hardness;
            if (hardness == 1)
                goal = 10;
            else if (hardness == 2)
                goal = 15;
            else
                goal = 20;
            numEnemiesDestroyed = 0;
            numEnemies = 0;
            setFocusable(true);
            setBackground(Color.BLACK);
            timer = new Timer(15, this);
            try {
                initBlocks();
            } catch (Exception e) {
            }

            addKeyListener(new KTAdapter());
            addMouseListener(new MTAdapter());
            addMouseMotionListener(new MTAdapter());
            tank1 = new Tank(Map.mapInitPlayerX.get(new Random().nextInt(Map.mapInitPlayerX.size())) * 16, Map.mapInitPlayerY * 16, true);
            setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            CollisionUtility.loadCollisionUtility(this, blocks, animations);
            BoardUtility.loadBoardUtility(enemy, blocks, animations, tank1, powerUps);
            timer.start();
        } else {
            this.isHost = isHost;
            if (this.isHost) {
                this.server = (Server) socket;
                this.server.setBoard(this);
                gameOver = false;
                win = false;
                pause = false;
                this.mainFrame = mainMenu1;
                this.stage = stage;
                this.hardness = hardness;
                if (hardness == 1)
                    goal = 10;
                else if (hardness == 2)
                    goal = 15;
                else
                    goal = 20;
                numEnemiesDestroyed = 0;
                numEnemies = 0;
                setFocusable(true);
                setBackground(Color.BLACK);
                timer = new Timer(15, this);
                try {
                    initBlocks();
                } catch (Exception e) {
                }
                addKeyListener(new KTAdapter());
                addMouseListener(new MTAdapter());
                addMouseMotionListener(new MTAdapter());
                tank1 = new Tank(Map.mapInitPlayerX.get(new Random().nextInt(Map.mapInitPlayerX.size())) * 16, Map.mapInitPlayerY * 16, true);
                tank2 = new Tank(Map.mapInitPlayerX.get(new Random().nextInt(Map.mapInitPlayerX.size())) * 16, Map.mapInitPlayerY * 16, false);
                setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                CollisionUtility.loadCollisionUtility(this, blocks, animations);
                BoardUtility.loadBoardUtility(enemy, blocks, animations, tank1, powerUps);
                timer.start();
            } else {
                this.client = (Client) socket;
                setBackground(Color.BLACK);
                timer = new Timer(15, this);
                timer.start();
            }
        }
    }

    /**
     * Initialize blocks according to the map.
     */
    public void initBlocks() throws Exception {
        SoundUtility.startStage();
        int[][] map = Map.getMap(stage);
        int type;
        for (int x = 0; x < map.length; x++) {
            for (int y = 0; y < map[0].length; y++) {
                type = map[x][y];
                BlockType bType = BlockType.getTypeFromInt(type);
                switch (bType) {
                    case BRICK:
                        blocks.add(new Brick(y * 16, x * 16));
                        break;
                    case STEEL:
                        blocks.add(new Steel(y * 16, x * 16));
                        break;
                    case RIVER:
                        blocks.add(new River(y * 16, x * 16));
                        break;
                    case TREE:
                        blocks.add(new Tree(y * 16, x * 16));
                        break;
                    case EDGE:
                        blocks.add(new Edge(y * 16, x * 16));
                        break;
                    default:
                        break;
                }
            }
        }
        setFrameSize();
    }

    private void setFrameSize() {
        Block lastB = blocks.get(blocks.size() - 1);
        mainFrame.setSize(new Dimension(lastB.x + lastB.width + 18, lastB.y + lastB.height + 47));
        mainFrame.setLocationRelativeTo(null);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (!singlePlayer && !isHost) {
            drawObjects(g);
        } else {
            drawObjects(g);
            drawEdge(g);
            endGame(g);
            winGame(g);
        }
        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * Draw objects on the board.
     */
    private void drawObjects(Graphics g) {
        if (!singlePlayer && !isHost) {
            for (Item item : allItems)
                g.drawImage(item.getImage(), item.getX(), item.getY(), this);
        } else {
            allItems = new ArrayList<>();
            for (TankAI tankAI : enemy) {
                if (tankAI.isVisible()) {
                    allItems.add(tankAI);
                    g.drawImage(tankAI.getImage(), tankAI.getX(), tankAI.getY(),
                            this);
                }
            }
            for (Bullet b : bullets) {
                if (b.isVisible()) {
                    allItems.add(b);
                    g.drawImage(b.getImage(), b.getX(), b.getY(), this);
                }
            }
            if (tank1.isVisible()) {
                allItems.add(tank1);
                g.drawImage(tank1.getImage(), tank1.getX(), tank1.getY(), this);
                g.setColor(Color.RED);
                Graphics2D g2 = (Graphics2D) g;
                g2.setStroke(new BasicStroke(3));
                g2.drawLine(tank1.getX() + tank1.width / 2, tank1.getY() + tank1.height / 2, tank1.gunX, tank1.gunY);
            }

            if (tank2 != null)
                if (tank2.isVisible()) {
                    allItems.add(tank2);
                    g.drawImage(tank2.getImage(), tank2.getX(), tank2.getY(), this);
                }
            for (Animation e : animations) {
                if (e.isVisible()) {
                    allItems.add(e);
                    g.drawImage(e.getImage(), e.getX(), e.getY(), this);
                }
            }
            for (Block a : blocks) {
                if (a.isVisible()) {
                    allItems.add(a);
                    g.drawImage(a.getImage(), a.getX(), a.getY(), this);
                }
            }
            for (PowerUp p : powerUps) {
                if (p.isVisible()) {
                    allItems.add(p);
                    g.drawImage(p.getImage(), p.getX(), p.getY(), this);
                }
            }
        }
    }

    /**
     * Draw the part that shows how many enemies left in the game on the edge of
     * the game board
     *
     * @param g                   Graphics
     * @param numEnemiesDestroyed number of enemies left in the game
     */
    private void drawEnemies(Graphics g, int numEnemiesDestroyed) {
        Image enemyIcon = loadImage("image/enemy.png");
        boolean up = false;
        int initY = 4;
        int initX = Map.mapWidth - (3 * 16);
        for (int i = 0; i < numEnemiesDestroyed; i++) {
            if (up) {
                up = !up;
                g.drawImage(enemyIcon, (initX + 16), (initY + i / 2) * 16, this);
                initY++;
            } else {
                up = !up;
                g.drawImage(enemyIcon, initX, (initY + i / 2 + 1) * 16, this);
            }
        }
    }


    /**
     * Draw the edge of the game
     *
     * @param g Graphics
     */
    private void drawEdge(Graphics g) {

        // Draw enemies
        drawEnemies(g, goal - numEnemiesDestroyed);
        // Draw Health
        drawHealth(g, tank1.getHealth());
        // Draw Lives
        drawLives(g, tank1.getLives());
        // Tank Level
        drawTankLevel(g, tank1.getStarLevel());
        // Draw Stage Number
        drawStage(g, stage);
        // Draw Bullet Number
        drawBullet(g, tank1.getBulletCount(), tank1.getRocketCount());

    }

    /**
     * Draw the part that shows Bullets of tank1 in the game on the edge of
     * the game board
     *
     * @param g           Graphics
     * @param bulletCount number of bullet we have
     * @param rocketCount number of rocket we have
     */
    private void drawBullet(Graphics g, int bulletCount, int rocketCount) {
        Font font = loadFont();
        g.setFont(font);
        String bullets = "B:" + String.format("%03d", bulletCount) + "  " + "R:" + String.format("%02d", rocketCount);
        g.drawString(bullets, 4 * 16, Map.mapHeight - 1 * 16);


    }

    /**
     * Draw the part that shows Level of tank1 in the game on the edge of
     * the game board
     *
     * @param g     Graphics
     * @param stage number of enemies left in the game
     */
    private void drawStage(Graphics g, Integer stage) {
        int initY = 4;
        int initX = Map.mapWidth - (8 * 16);
        g.drawString("STAGE:" + String.valueOf(stage), initX, 2 * 16);
    }

    /**
     * Draw the part that shows Level of tank1 in the game on the edge of
     * the game board
     *
     * @param g         Graphics
     * @param starLevel star Level of tank1 in the game
     */
    private void drawTankLevel(Graphics g, int starLevel) {
        Font font = loadFont();
        g.setFont(font);
        g.setColor(Color.WHITE);
        Image live = loadImage("image/Star.png");
        g.drawString("LEVEL:", 4 * 16, Map.mapHeight - 2 * 16);
        int i;
        for (i = 0; i < starLevel + 1; i++) {
            g.drawImage(live, (i + 10) * 16, Map.mapHeight - 3 * 16, this);
        }
    }

    /**
     * Draw the part that shows Lives of tank1 in the game on the edge of
     * the game board
     *
     * @param g     Graphics
     * @param lives Lives of tank1 in the game
     */
    private void drawLives(Graphics g, int lives) {
        Font font = loadFont();
        g.setFont(font);
        g.setColor(Color.WHITE);
        Image live = loadImage("image/lives.png");
        g.drawString("LIVES:", 4 * 16, 3 * 16);
        int i;
        for (i = 0; i < lives; i++) {
            g.drawImage(live, (i + 11) * 16, 2 * 16, this);
        }
    }

    /**
     * Draw the part that shows heath of tank1 in the game on the edge of
     * the game board
     *
     * @param g      Graphics
     * @param health Health of tank1 in the game
     */
    private void drawHealth(Graphics g, int health) {
        Font font = loadFont();
        g.setFont(font);
        g.setColor(Color.WHITE);
        Image hGreen = loadImage("image/HealthBar_Green.png");
        Image hRed = loadImage("image/HealthBar_Red.png");
        g.drawString("HEALTH:", 4 * 16, 2 * 16);
        int i;
        for (i = 0; i < health; i++) {
            g.drawImage(hGreen, (i + 11) * 16, 1 * 16, this);
        }
        for (; i < tank1.MAX_HEALTH; i++) {
            g.drawImage(hRed, (i + 11) * 16, 1 * 16, this);
        }
    }


    /**
     * Load images given the image address
     *
     * @param imageAddress image address to the demand image
     * @return image
     */
    public Image loadImage(String imageAddress) {
        ImageIcon icon = new ImageIcon(imageAddress);
        return icon.getImage();
    }


    public GameBoard(MainMenu mainMenu, int hardness, boolean singlePlayer, boolean isHost, Object socket) throws Exception {
        this(mainMenu, hardness, 1, singlePlayer, isHost, socket);
    }


    /**
     * UpdatesItems is used to call the various update calls.
     */
    public void updateItems() {
        spawnTankAI();
        spawnPowerUp();
        updateTank();
        updateTankAI();
        updateBullets();
        updateBlocks();
        updateAnimations();
        updateBlocks();
        updatePowerUps();
    }

    /**
     * Updates the tank1 AI.
     */
    private void updateTankAI() {
        for (TankAI tankAI : enemy) {
            if (tankAI.isVisible()) {
                if ("easy".equals(tankAI.getDifficulty())) {
                    tankAI.actionEasy();
                } else if ("normal".equals(tankAI.getDifficulty())) {
                    tankAI.actionNormal(this.tank1);
                } else if ("hard".equals(tankAI.getDifficulty())) {
                    tankAI.actionHard(this.tank1);
                }
            }
        }
        for (int i = 0; i < enemy.size(); i++) {
            if (enemy.get(i).vis == false) {
                enemy.remove(i);
            }
        }
    }

    /**
     * Updates the powerUps on the board
     * <p>
     * Unlike the other updateMethods, update for powerUps handles the collision
     * of a player tank1 and a powerUp PowerUps are removed if vis = false
     * otherwise they are updated via updateAnimations.
     */
    private void updatePowerUps() {
        BoardUtility.updatePowerUps();
    }

    private void spawnPowerUp() {
        BoardUtility.spawnPowerUp();
    }


    /**
     * Spawn tank1 AI to reach the goal.
     */
    private void spawnTankAI() {
        while (numEnemies < goal) {
            if (enemy.size() < 4) {
                boolean powerUp;
                powerUp = (numEnemies % 4 == 1);
                if (numEnemies < (goal / 5)) {
                    BoardUtility.spawnTankAI("easy", powerUp);
                } else if (numEnemies >= (goal / 5) && numEnemies < (goal / 2)) {
                    BoardUtility.spawnTankAI("normal", powerUp);
                } else if (numEnemies >= (goal / 2)) {
                    BoardUtility.spawnTankAI("hard", powerUp);
                }
                numEnemies++;
            } else {
                return;
            }
        }
    }

    public int inCreaseNumEnemiesDestroyed() {
        return numEnemiesDestroyed++;
    }

    /**
     * Update animations on the board this includes
     * Explosion/ExplodingTank/TankSpawn
     * <p>
     * Animations are removed if vis is false. Otherwise animations are updated
     * via the updateAnimation method
     */
    public void updateAnimations() {
        BoardUtility.updateAnimations();
    }

    /**
     * updates the bullets for both the player tank1 and enemyIcon Tanks
     */
    private void updateBullets() {
        BoardUtility.updateBulletsTankAI();
        BoardUtility.updateBulletsTank();
        bullets = new ArrayList<>();
        bullets.addAll(tank1.getBullets());
        for (TankAI tankAI : enemy) {
            bullets.addAll(tankAI.getBullets());
        }
    }

    /**
     * Check collisions between different Items classes
     */
    public void checkCollisions() {
        BoardUtility.checkCollisions();
    }

    /**
     * Update blocks on the board this includes Base/Brick/Edge/River/Steel/Tree
     * <p>
     * Blocks are removed if vis is false. Blocks that are types RIVER
     * they will be updated via the updateAnimation method
     */
    private void updateBlocks() {
        BoardUtility.updateBlocks();
    }


    /**
     * Updates the player tank1.
     * <p>
     * If the tank1 is visible it is moved
     */
    private void updateTank() {
        BoardUtility.updateTank();
        tank1.syncGun();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!isSinglePlayer() && !isHost) {
            getItems();
        } else {
            if (pause) {
                System.out.println(pause);
                tank1.stopFireing();
                timer.stop();
                return;
            }
            if (gameOver || win) {
                tank1.stopFireing();
                timer.stop();
                return;
            }
            updateItems();
            checkCollisions();
            checkGameOver();
            nextStage();
        }
        repaint();
    }


    /**
     * getItemsFromHost
     */
    private void getItems() {
        allItems = client.getItems();
    }


    /**
     * Load the game font to the program
     *
     * @return font of the game
     */
    public static Font loadFont() {
        Font font = null;
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, new File("prstart.ttf"));
            font = font.deriveFont(java.awt.Font.PLAIN, 15);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(font);

        } catch (FontFormatException | IOException ex) {
        }
        return font;
    }

    public void unPause() {
        gameOver = false;
        win = false;
        pause = false;
        if (!pause) {
            timer.start();
        }
        setFrameSize();
    }

    public void pauseAndBackToMenu() {
        tank1.stopFireing();
        timer.stop();
        mainFrame.setGame(this);
        backToMenu();
    }

    /**
     * Tank key adapter. Override the methods in KeyAdapter to add events
     * handlers for the tanks
     */
    private class KTAdapter extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {
            tank1.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == e.VK_P || e.getKeyCode() == e.VK_ESCAPE) {
                pause = !pause;
                if (!pause)
                    timer.start();
                if (e.getKeyCode() == e.VK_ESCAPE)
                    pauseAndBackToMenu();
            }
            tank1.keyPressed(e);
        }

        @Override
        public void keyTyped(KeyEvent e) {
            checkCheat(e.getKeyChar());
        }
    }

    /**
     * Tank Mouse adapter. Override the methods in MouseAdapter to add events
     * handlers for the tanks
     */
    private class MTAdapter extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            tank1.mousePressed(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            tank1.mouseReleased(e);
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            tank1.mouseClicked(e);
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            tank1.mouseMoved(e);
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            tank1.mouseMoved(e);
        }
    }

    /**
     * Check if the RemainAI Tank Or the player's Lives And the player's health is lower than 0. If lower than 0, then end
     * the game
     */
    private void checkGameOver() {
        if (tank1.getLives() < 0)
            setEndGame();
    }

    /**
     * Set the gameOver variable to true.
     */
    public static void setEndGame() {
        SoundUtility.gameOver();
        System.out.println("Game Over Played");
        gameOver = true;
    }

    /**
     * go to Stage
     */
    public void nextStage() {
        if (enemy.isEmpty()) {
            try {
                stage += 1;
                if (hardness == 1)
                    goal = 10;
                else if (hardness == 2)
                    goal = 15;
                else
                    goal = 20;
                numEnemies = 0;
                numEnemiesDestroyed = 0;
                clearBoard();
                initBlocks();
                CollisionUtility.resetTankPosition(tank1, false);
                tank1.addAmmo();
                tank1.addAmmo();
                CollisionUtility.loadCollisionUtility(blocks, animations);
                BoardUtility.loadBoardUtility(enemy, blocks, animations, tank1, powerUps);
            } catch (Exception e) {
                win = true;
                SoundUtility.statistics();
                System.out.println("You win!");
            }
        }
    }


    /**
     * back to menu
     */
    public static void backToMenu() {
        Menu menu = new Menu(mainFrame);
        mainFrame.getGamePanel().removeAll();
        mainFrame.getGamePanel().add(menu);
        menu.revalidate();
        menu.repaint();
        mainFrame.setVisible(true);
        mainFrame.setSize(new Dimension(600, 600));
        mainFrame.setLocationRelativeTo(null);
        menu.requestFocusInWindow();
    }


    /**
     * Create end game information on the screen. After the "GAME OVER" label
     * shows on the screen, a score board of the entire game will be displayed
     *
     * @param g Graphics
     */
    public void winGame(Graphics g) {
        if (win) {
            Timer gameOverTimer = new Timer(80, new ActionListener() {
                @Override
                public void actionPerformed(
                        ActionEvent e) {
                    yPos += direction;
                    if (yPos == stopYPos) {
                        direction = 0;
                    } else if (yPos > getHeight()) {
                        yPos = getHeight();
                    } else if (yPos < 0) {
                        yPos = 0;
                        direction *= -1;
                    }
                    repaint();
                }
            });
            gameOverTimer.setRepeats(true);
            gameOverTimer.setCoalesce(true);
            gameOverTimer.start();
            Font font = loadFont();
            g.setFont(font);
            g.setColor(Color.GREEN);
            g.drawString("YOU WIN!!!", Map.mapWidth / 2 - 85, yPos);
            if (yPos == stopYPos) {
                gameOverTimer.stop();
                Timer sorceBoardTimer = new Timer(2000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        mainFrame.setGame(null);
                        backToMenu();
                    }
                });
                sorceBoardTimer.setRepeats(false);
                sorceBoardTimer.start();
            }
        }
    }

    /**
     * Create end game information on the screen. After the "GAME OVER" label
     * shows on the screen, a score board of the entire game will be displayed
     *
     * @param g Graphics
     */
    public void endGame(Graphics g) {

        if (gameOver) {
            Timer gameOverTimer = new Timer(80, new ActionListener() {
                @Override
                public void actionPerformed(
                        ActionEvent e) {
                    yPos += direction;
                    if (yPos == stopYPos) {
                        direction = 0;
                    } else if (yPos > getHeight()) {
                        yPos = getHeight();
                    } else if (yPos < 0) {
                        yPos = 0;
                        direction *= -1;
                    }
                    repaint();
                }
            });
            gameOverTimer.setRepeats(true);
            gameOverTimer.setCoalesce(true);
            gameOverTimer.start();
            Font font = loadFont();
            g.setFont(font);
            g.setColor(Color.RED);
            g.drawString("GAME OVER", Map.mapWidth / 2 - 85, yPos);

            if (yPos == stopYPos) {
                gameOverTimer.stop();
                Timer sorceBoardTimer = new Timer(3000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        mainFrame.setGame(null);
                        backToMenu();
                    }
                });
                sorceBoardTimer.setRepeats(false);
                sorceBoardTimer.start();
            }
        }
    }

    /**
     * Clear the initialized variables on the board.
     */
    public void clearBoard() {
        animations = new ArrayList<>();
        blocks = new ArrayList<>();
        powerUps = new ArrayList<>();
        updateItems();
        CollisionUtility.loadCollisionUtility(blocks, animations);
    }

    private void checkCheat(char c){
        cheatBuffer+=c;
        while (cheatBuffer.length()>10)
            cheatBuffer=cheatBuffer.substring(1);
        cheatBuffer=cheatBuffer.toLowerCase();
        if(cheatBuffer.indexOf("ammo")!=-1) {
            SoundUtility.powerupPick();
            tank1.addAmmo();
            cheatBuffer="";
        }
        if(cheatBuffer.indexOf("fullh")!=-1) {
            SoundUtility.powerupPick();
            tank1.resetHealth();
            cheatBuffer="";
        }
        if(cheatBuffer.indexOf("getstar")!=-1) {
            SoundUtility.powerupPick();
            tank1.upStarLevel();
            cheatBuffer="";
        }
    }

}
