package Main;

import Items.Item;
import Items.PictureID;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;


public class ClientManager implements Runnable {
    Socket clientHolder;
    Server serverHolder;
    InputStream fromClientStream;
    OutputStream toClientStream;
    DataInputStream reader;
    PrintWriter writer;
    String name;
    GameBoard board;

    public ClientManager(Server server, Socket client) {
        serverHolder = server;
        clientHolder = client;
    }

    public void setBoard(GameBoard board) {
        this.board = board;
    }

    public void run() {
        try {
            // input stream (stream from client)
            fromClientStream = clientHolder.getInputStream();
            // output stream (stream to client)
            toClientStream = clientHolder.getOutputStream();
            reader = new DataInputStream(fromClientStream);
            writer = new PrintWriter(toClientStream, true);
            String msg = reader.readLine();
            if (msg.equals("GetHardness")) {
                writer.println(serverHolder.getGameHardness());
                return;
            }
            if (msg.equals("I Want To Play")) {
                msg = reader.readLine();

                if (0 == JOptionPane.showConfirmDialog(null, "Do you want to play with " + msg + " ?", "play?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)) {
                    writer.println("yes");
                } else {
                    writer.println("no");
                    return;
                }
            }
            serverHolder.setClientManager(this);
            while (true) {
                // read command from client
                String command = reader.readLine();

                if (command.equals("Items")) {
                    ArrayList<Item> items = (ArrayList<Item>) board.getAllItems().clone();
                    int[][] itemss = new int[items.size()][3];
                    int i=0;
                    for (Item item : items) {
                        itemss[i][0]=item.getX();
                        itemss[i][1]=item.getY();
                        itemss[i][2]=PictureID.getID(item.imageName);
                    }
                    ObjectOutputStream os = new ObjectOutputStream(toClientStream);
                    os.writeObject(itemss);
                    os.flush();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
