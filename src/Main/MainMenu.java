package Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;

public class MainMenu extends JFrame {
    private GameBoard game;
    private JPanel gamePanel;

    public void setGame(GameBoard game) {
        this.game = game;
    }

    public boolean haveUnFinishedGame(){
        return game!=null;
    }

    public void unPause(){
        try {
            getGamePanel().removeAll();
            getGamePanel().add(game);
            game.revalidate();
            game.repaint();
            game.loadGame();
            game.unPause();
            game.requestFocusInWindow();
            setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public MainMenu() throws HeadlessException {
        super("JTank");
        start();
        gamePanel = new JPanel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(600, 600));

        gamePanel.setMinimumSize(new Dimension(600, 600));
        gamePanel.setLayout(new GridLayout(1, 0));

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(gamePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(gamePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(game!=null)
                {
                    System.out.println(game);
                    try {
                        ObjectOutputStream writer = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(System.getProperty("user.home") + "\\Tank\\lastGame.dat")));
                        writer.writeObject(game);
                        writer.close();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        pack();
    }

    private void start() {
        try {
            if (!(new File(System.getProperty("user.home") + "\\Tank").exists()))
                new File(System.getProperty("user.home") + "\\Tank").mkdir();
            File f = new File(System.getProperty("user.home") + "\\Tank\\lastGame.dat");
            if (f.exists()) {
                ObjectInputStream reader = new ObjectInputStream(
                        new BufferedInputStream(
                                new FileInputStream(
                                        System.getProperty("user.home") + "\\Tank\\lastGame.dat")));
                game = (GameBoard) reader.readObject();
                game.setMainFrame(this);
                reader.close();
            } else
                game = null;
        } catch (Exception e) {
            e.printStackTrace();
            game = null;
        }
    }

    public static void main(String[] args) {
        MainMenu mainMenu = new MainMenu();
        Menu menu = new Menu(mainMenu);
        mainMenu.getGamePanel().add(menu);
        mainMenu.setVisible(true);
        mainMenu.setLocationRelativeTo(null);
    }

    /**
     * Get the game JPanel
     *
     * @return gamePanel
     */
    public JPanel getGamePanel() {
        return gamePanel;
    }


}
