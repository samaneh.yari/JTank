package Main;

import javax.swing.*;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Server implements Runnable {
    ServerSocket mServer;
    int serverPort;
    int gameHardness;
    Menu menu;
    Socket client;
    ArrayList<Thread> threads = new ArrayList<Thread>();
    ClientManager clientsMng;

    public Server(Menu menu, int serverPort,int gameHardness) {
        this.menu =menu;
        this.gameHardness =gameHardness;
        this.serverPort = serverPort;
        try {
            // create server socket!
            mServer = new ServerSocket(serverPort);
            System.out.println("Server Created!");
        } catch (IOException e) {
        }
    }

    public int getGameHardness() {
        return gameHardness;
    }


    @Override
    public void run() {
        try {
            String msg = null;
            while (true) {
                Socket client = mServer.accept();
                System.out.println("Connected to New Client!");

                Thread t = new Thread(new ClientManager(this, client));
                // add Thread to "threads" list
                threads.add(t);
                t.start();
            }
        } catch (IOException e) {
        }
    }

    public void close() {
        try {
            mServer.close();
            System.out.println("server Terminated");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setBoard(GameBoard board){
        clientsMng.setBoard(board);
    }

    public void setClientManager( ClientManager clientManager) {
        this.clientsMng=clientManager;
        menu.startMultiPlayer(true,this);
    }
}

