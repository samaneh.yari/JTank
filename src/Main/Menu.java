package Main;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import javax.swing.*;

/**
 * A class for the menu of the game
 */
public class Menu extends JPanel implements ActionListener, KeyListener {
    public Image background, tank, tree;
    public MainMenu mainFrame;
    private int yPos = 100;
    private int choise = 0, minChoise = 0, maxChoise = 2;
    private int time = 0;
    private String[][] selectiveOptions = {{"1 PLAYER", "2 PLAYER", "Cunstruction"},
            {"Continue", "Easy", "Normal", "Hard"}, {"Host", "Join"}, {}};
    private ArrayList<Integer> select = new ArrayList<>();
    Server server;
    Thread serverThread;
    Timer timerServer;
    private int portServer;
    private static boolean menuStatus = true;

    /**
     * Constructor for the menu
     *
     * @param mainFrame
     */
    public Menu(MainMenu mainFrame) {
        this.mainFrame = mainFrame;
        this.setBackground(Color.BLACK);
        this.addKeyListener(this);
        this.setFocusable(true);
        this.setLayout(null);
        portServer = 9090;
        loadMenuImages();
    }

    private void loadMenuImages() {
        background = loadImage("image/JTank.png");
        tank = loadImage("image/playerTank_right.png");
        tree = loadImage("image/trees2.png");

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (select.size() == 0)
            drawMenu(g, 0);
        else {
            if (select.size() == 1 && (select.get(0) == 0 || select.get(0) == 1))
                drawMenu(g, 1);
            if (select.size() == 2 && select.get(0) == 1)
                drawMenu(g, 2);
            if (select.size() == 3)
                drawMenu(g, 3);
        }
        drawMenuComponents(g);
    }

    private void drawMenu(Graphics g, int n) {
        maxChoise = selectiveOptions[n].length - 1;
        Font font = loadFont();
        g.drawImage(background,
                getWidth() / 2 - background.getWidth(null) / 2 - 10,
                yPos, this);
        g.setFont(font);
        g.setColor(Color.WHITE);
        for (int i = 0; i < selectiveOptions[n].length; i++) {
            g.drawString(selectiveOptions[n][i].toUpperCase(), getWidth() / 2 - 56,
                    yPos + background.getHeight(null) + 50 + i * 30);
        }
    }


    private void drawMenuComponents(Graphics g) {
        g.drawImage(tree, 10, 50, this);
        g.drawImage(tree, 10, 90, this);
        g.drawImage(tank, getWidth() / 2 - 90,
                yPos + background.getHeight(null) + 25 + choise * 30, this);
        Font font = loadFont();
        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString("PRESS ENTER(ESCAPE FOR BACK)",
                getWidth() / 2 - 200,
                getHeight() * 4 / 5 + 25);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    /**
     * Load the game font to the program
     *
     * @return font of the game
     */
    public static Font loadFont() {
        Font font = null;
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, new File("prstart.ttf"));
            font = font.deriveFont(java.awt.Font.PLAIN, 15);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(font);

        } catch (FontFormatException | IOException ex) {
        }
        return font;
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == e.VK_ENTER) {
            select.add(choise);
            if (select.size() == 1)
                if (select.get(0) == 2) {
                    loadMapMaker();
                }
            if (select.size() == 2)
                if (select.get(0) == 0) {
                    if (mainFrame.haveUnFinishedGame() && (select.get(1) == 0))
                        mainFrame.unPause();
                    else if ((!mainFrame.haveUnFinishedGame() && (select.get(1) == 0)))
                        select.remove(select.size() - 1);
                    else
                        loadGameBoard(select.get(1), true, false, null);
                }
            if (select.size() == 3)
                if (select.get(2) == 0)
                    makeHost(select.get(1) + 1);
            if (select.size() == 3)
                if (select.get(2) == 1)
                    startSearching();
            if (select.size() == 4)
                if (selectiveOptions[3].length != 0) {
                    stopSearching();
                    tryToStart(selectiveOptions[3][select.get(3)]);
                }
            choise = 0;
            repaint();
        } else if (e.getKeyCode() == e.VK_ESCAPE) {
            if (select.size() == 3)
                if (select.get(2) == 0)
                    closeHost();
            if (select.size() == 3)
                if (select.get(2) == 1)
                    stopSearching();
            select.remove(select.size() - 1);
            choise = 0;
            repaint();
        } else if (e.getKeyCode() == e.VK_UP || e.getKeyCode() == e.VK_W)
            goUp();
        else if (e.getKeyCode() == e.VK_DOWN || e.getKeyCode() == e.VK_S)
            goDown();
    }

    private void loadMapMaker() {
        try {
            menuStatus = false;
            MapMaker panel = new MapMaker(mainFrame);
            mainFrame.getGamePanel().removeAll();
            panel.revalidate();
            panel.repaint();
            mainFrame.getGamePanel().add(panel);
            panel.requestFocusInWindow();
            mainFrame.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void tryToStart(String selectedItem) {
        Client c = Client.tryToStart(selectedItem, portServer);
        if (c != null)
            startMultiPlayer(false, c);
    }


    private void stopSearching() {
        timerServer.stop();
    }

    private void startSearching() {
        selectiveOptions[3] = new String[0];
        timerServer = new Timer(5000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                repaint();
                new IPTester(portServer, select.get(1) + 1).execute();
            }
        });
        timerServer.start();
    }

    private void closeHost() {
        timerServer.stop();
        if (serverThread != null) {
            serverThread.stop();
            serverThread = null;
        }
        if (server != null) {
            server.close();
            server = null;
        }
    }

    private void makeHost(int hardness) {
        time = 0;
        selectiveOptions[3] = new String[1];
        selectiveOptions[3][0] = String.format("%02d:%02d", (time / 60), (time % 60));
        timerServer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                repaint();
                selectiveOptions[3] = new String[1];
                selectiveOptions[3][0] = String.format("%02d:%02d", (time / 60), (time % 60));
                time++;
            }
        });
        timerServer.start();
        server = new Server(this, portServer, hardness);
        serverThread = new Thread(server);
        serverThread.start();
    }

    /**
     * start MultyPlayer Game.
     */
    public void startMultiPlayer(boolean isHost, Object Socket) {
        loadGameBoard(select.get(1) + 1, false, isHost, Socket);
    }


    /**
     * Load the board to the game panel on the JFrame of the game.
     */
    public void loadGameBoard(int hardness, boolean singlePlayer, boolean isHost, Object socket) {
        try {
            menuStatus = false;
            GameBoard panel = new GameBoard(mainFrame, hardness, singlePlayer, isHost, socket);
            mainFrame.getGamePanel().removeAll();
            panel.revalidate();
            panel.repaint();
            mainFrame.getGamePanel().add(panel);
            panel.requestFocusInWindow();
            mainFrame.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void goUp() {
        if (choise > minChoise)
            choise--;
        repaint();
    }

    private void goDown() {
        if (choise < maxChoise)
            choise++;
        repaint();
    }


    /**
     * Return if the game is showing the menu
     *
     * @return a boolean
     */
    public static boolean getMenuStatus() {
        return menuStatus;
    }

    /**
     * Load images given the image address
     *
     * @param imageAddress image address to the demand image
     * @return image
     */
    public Image loadImage(String imageAddress) {
        ImageIcon icon = new ImageIcon(imageAddress);
        return icon.getImage();
    }

    /**
     * a SwingWorker is a class for find Enabled Ip with open port server inbackground
     */
    class IPTester extends SwingWorker<ArrayList<String>, Void> {
        int port;
        int hardness;

        public IPTester(int port, int hardness) {
            super();
            this.port = port;
            this.hardness = hardness;

        }

        @Override
        protected void done() {
            try {
                ArrayList<String> enabledIP = get();
                for (int i = enabledIP.size() - 1; i >= 0; i--) {
                    if (hardness != Client.getHostGameHardness(enabledIP.get(i), port))
                        enabledIP.remove(i);
                }

                selectiveOptions[3] = new String[enabledIP.size()];
                for (int i = 0; i < enabledIP.size() && i < 5; i++)
                    selectiveOptions[3][i] = enabledIP.get(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected ArrayList<String> doInBackground() throws Exception {
            ArrayList<String> enabledIP = new ArrayList<>();
            final String myIp;
            try {
                myIp = InetAddress.getLocalHost().getHostAddress();
                System.out.println(myIp);
            } catch (Exception e) {
                return enabledIP;
            }
            for (int i = 1; i <= 254; i++) {
                final int j = i;  // i as non-final variable cannot be referenced from inner class
                new Thread(new Runnable() {   // new thread for parallel execution
                    public void run() {
                        try {
                            String adress = myIp.substring(0, myIp.lastIndexOf('.')) + "." + j;
                            if (isReachable(adress, portServer, 1000)) {
                                enabledIP.add(adress);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
            new Thread(new Runnable() {   // new thread for parallel execution
                public void run() {
                    try {
                        String adress = "127.0.0.1";
                        if (isReachable(adress, port, 1000)) {
                            enabledIP.add(adress);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            return enabledIP;
        }
    }


    private static boolean isReachable(String addr, int openPort, int timeOutMillis) {
        try {
            try (Socket soc = new Socket()) {
                soc.connect(new InetSocketAddress(addr, openPort), timeOutMillis);
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}
