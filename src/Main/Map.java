package Main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class Map {
    /**
     * Hard coded the size of the board
     */
    public static int mapWidth;
    public static int mapHeight;
    public static ArrayList<Integer> mapInitPlayerX;
    public static int mapInitPlayerY;
    public static ArrayList<Integer> mapInitEnemyX;
    public static ArrayList<Integer> mapInitConstantEnemyX;
    public static ArrayList<Integer> mapInitConstantEnemyY;
    public static Integer mapInitEnemyY;
    public static final int shiftTop = 4;
    public static final int shiftDown = 4;
    public static final int shiftLeft = 2;
    public static final int shiftRight = 4;


    /**
     * Return the map in 2D array
     *
     * @param stage
     * @return 2D array that represents the map
     */
    public static int[][] getMap(int stage) throws Exception {
        return createNewStageMap(stage);
    }

    /**
     * Create map of the new stage given the stage number
     *
     * @param stage current stage number
     * @return a 2D array that represents the map of the given stage
     */
    private static int[][] createNewStageMap(int stage) throws Exception {
        ArrayList<ArrayList<Integer>> levelReadFromFile = readFromFile("stages/" + String.valueOf(stage) + ".JtankMap");
        int[][] array = arrayListToArray(levelReadFromFile);
        int[][] map = new int[array.length + shiftRight + shiftDown][array[0].length + shiftLeft + shiftRight];
        for (int[] rows : map)
            Arrays.fill(rows, 5);
        for (int i = 0; i < array.length; i++)
            for (int j = 0; j < array[0].length; j++)
                map[i + shiftTop][j + shiftLeft] = array[i][j];
        mapWidth = map[0].length * 16;
        mapHeight = map.length * 16;
        if (!setPlayersInitXY(array))
            throw new Exception("cant find init place for tank");
        if (!setEnemyInitXY(array))
            throw new Exception("cant find init place for tank");
        if (!mapInitConstantEnemyXY(array))
            throw new Exception("cant find init place for Constant tank");
        return map;
    }

    /**
     * set Enemy Initial Position for further operation.
     *
     * @param map 2D Array of Exported map
     * @return an Boolean to show Success FindPosition
     */
    private static boolean mapInitConstantEnemyXY(int[][] map) {
        int i, j = 0;
        mapInitConstantEnemyX = new ArrayList<>();
        mapInitConstantEnemyY = new ArrayList<>();
        for (j = 4 + shiftTop; j < map.length - 4 - shiftDown; j += 2)
            for (i = 2; i < map[0].length - 2 - shiftRight; i++) {
                if (map[j][i] == 0 && map[j][i + 1] == 0 && map[j + 1][i] == 0 && map[j + 1][i + 1] == 0) {
                    mapInitConstantEnemyX.add(i + shiftLeft);
                    mapInitConstantEnemyY.add(j + shiftTop);
                    i++;
                }
            }
        if (mapInitEnemyX.size() <= 0)
            return false;
        return true;
    }


    /**
     * set Enemy Initial Position for further operation.
     *
     * @param map 2D Array of Exported map
     * @return an Boolean to show Success FindPosition
     */
    private static boolean setEnemyInitXY(int[][] map) {
        int i, j = 0;
        mapInitEnemyX = new ArrayList<>();
        for (i = 2; i < map[0].length - 2; i++) {
            if (map[j][i] == 0 && map[j][i + 1] == 0 && map[j + 1][i] == 0 && map[j + 1][i + 1] == 0) {
                mapInitEnemyX.add(i + shiftLeft);
                i++;
            }
        }
        if (mapInitEnemyX.size() > 0) {
            mapInitEnemyY = j + shiftTop;
        } else return false;
        return true;
    }


    /**
     * set Player Initial Position for further operation.
     *
     * @param map and 2D Array of Exported map
     * @return an Boolean to show Success FindPosition
     */
    private static Boolean setPlayersInitXY(int[][] map) {
        int i, j = map.length - 2;
        mapInitPlayerX = new ArrayList<>();
        for (i = 4; i < map[0].length - 3; i++) {
            if (map[j][i] == 0 && map[j][i + 1] == 0 && map[j + 1][i] == 0 && map[j + 1][i + 1] == 0) {
                mapInitPlayerX.add(i + shiftLeft);
                i++;
            }
        }
        if (mapInitPlayerX.size() > 0) {
            mapInitPlayerY = j + shiftTop;
        } else return false;
        return true;
    }

    /**
     * Convert arrayList to array for further operation.
     *
     * @param arrayList
     * @return an array converted from arrayList
     */
    private static int[][] arrayListToArray(ArrayList<ArrayList<Integer>> arrayList) {
        int[][] intArray = new int[arrayList.size()][arrayList.get(0).size()];
        for (int i = 0; i < arrayList.size(); i++) {
            ArrayList<Integer> temp = arrayList.get(i);
            for (int j = 0; j < temp.size(); j++)
                intArray[i][j] = temp.get(j);
        }
        return intArray;
    }

    /**
     * Read map from files
     *
     * @param fileName
     * @return an array list that represents a map
     */
    private static ArrayList<ArrayList<Integer>> readFromFile(String fileName) throws Exception {
        ArrayList<ArrayList<Integer>> tempMap = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String currentLine;
            while ((currentLine = br.readLine()) != null) {
                if (currentLine.isEmpty()) {
                } else {
                    ArrayList<Integer> row = new ArrayList<>();
                    String[] values = currentLine.trim().split("");
                    for (String string : values) {
                        if (!string.isEmpty()) {
                            switch (string) {
                                case "#":
                                    row.add(1);
                                    break;
                                case "@":
                                    row.add(2);
                                    break;
                                case "%":
                                    row.add(4);
                                    break;
                                case "~":
                                    row.add(3);
                                    break;
                                default:
                                    row.add(0);
                                    break;
                            }
                        }
                    }
                    tempMap.add(row);
                }
            }
        } catch (IOException e) {
            throw new Exception("Stage File cant Found " + fileName);
        }
        return tempMap;
    }

    public static int[][] getBlankMap() {
        int[][] array = new int[40][40];
        int[][] map = new int[40 + shiftRight + shiftDown][40 + shiftLeft + shiftRight];
        for (int[] rows : map)
            Arrays.fill(rows, 5);
        for (int i = 0; i < array.length; i++)
            for (int j = 0; j < array[0].length; j++)
                map[i + shiftTop][j + shiftLeft] = array[i][j];
        mapWidth = map[0].length * 16;
        mapHeight = map.length * 16;
        return map;
    }
}
