package Main;

import Items.Item;
import Items.PictureID;
import Items.Tank;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Client {
    Socket mSocket;
    int port;
    String serverAddress;
    InputStream fromServerStream;
    OutputStream toServerStream;
    DataInputStream reader;
    PrintWriter writer;

    public Client(String serverAddress, int port) {
        try {
            this.serverAddress = serverAddress;
            this.port = port;
            mSocket = new Socket(serverAddress, port);
            fromServerStream = mSocket.getInputStream();
            toServerStream = mSocket.getOutputStream();
            reader = new DataInputStream(fromServerStream);
            writer = new PrintWriter(toServerStream, true);
        } catch (UnknownHostException e) {
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.exit(3);
        }
    }

    public static int getHostGameHardness(String ip, int port) {
        Client c = new Client(ip, port);
        return c.start2();
    }

    public static Client tryToStart(String ip, int port) {
        Client c = new Client(ip, port);
        if (c.start())
            return c;
        return null;
    }

    public boolean start() {
        try {
            System.out.println("connect to server for Play " + serverAddress + ":" + port);
            sendMSG("I Want To Play");
            sendMSG(System.getProperty("user.name"));
            return reader.readLine().equals("yes");
        } catch (UnknownHostException e) {
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return false;
    }

    public int start2() {
        try {
            System.out.print(serverAddress + ":" + port + " Check Hardness: ");
            sendMSG("GetHardness");
            String msg = reader.readLine();
            System.out.println(msg);
            return Integer.parseInt(msg);
        } catch (UnknownHostException e) {
        } catch (IOException e) {
            return -1;
        }
        return -1;
    }

    private void sendMSG(String name) {
        writer.println(name);
    }

    public ArrayList<Item> getItems() {
        sendMSG("Items");
        try {
            int count = Integer.parseInt(reader.readLine());
            ArrayList<Item> items = new ArrayList<>();
            ObjectInputStream is = new ObjectInputStream(fromServerStream);
            int[][] m = (int[][]) is.readObject();
            for (int[] temp : m) {
                items.add(new Item(temp[0],temp[1],PictureID.getPicAdrress(temp[2])));
            }
            return items;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
