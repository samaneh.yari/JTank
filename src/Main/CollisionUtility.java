package Main;


import Items.*;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Random;

/**
 * Utility class for collision detection and handling
 */
public class CollisionUtility {
    // Instance variable that indicated the x, y coordinates of the powerUp
    public static int powerUpX = 0;
    public static int powerUpY = 0;
    private static ArrayList<Block> blocks;
    private static ArrayList<Animation> explosions;
    private static GameBoard gameBoard;
    // Instance variable that tracks the number of enemy tanks being destroyed
    private static int[] enemyTankNum = {0, 0, 0, 0};

    /**
     * Load blocks and explosion animation from the input array list
     *
     * @param board
     * @param inblocks    input blocks
     * @param inexplosion
     */
    static public void loadCollisionUtility(GameBoard board, ArrayList<Block> inblocks,
                                            ArrayList<Animation> inexplosion) {
        gameBoard=board;
        blocks = inblocks;
        explosions = inexplosion;
    }

    /**
     * Load blocks and explosion animation from the input array list
     *
     * @param inblocks input blocks
     * @param inexplosion
     */
    static public void loadCollisionUtility(ArrayList<Block> inblocks,
                                            ArrayList<Animation> inexplosion) {
        blocks = inblocks;
        explosions = inexplosion;
    }

    /**
     * Reset the score of the game after game over.
     */
    public static void resetScore() {
        enemyTankNum = new int[]{0, 0, 0, 0};
    }

    /**
     * Helper method for collision between bullets and blocks
     *
     * @param bullet
     * @param block
     */
    public static void CollisionBulletsBlocksHelper(Bullet bullet, Block block) {
        BlockType type = BlockType.getTypeFromInt(block.getType());
        if (type.equals(BlockType.BRICK)) {
            block.lowerHealth(bullet.getDamage());
            bullet.setVisible(false);
        }
        if (type.equals(BlockType.STEEL) && bullet.getUpgrade()) {
            block.lowerHealth(bullet.getDamage());
            bullet.setVisible(false);
        }
        if (type.equals(BlockType.STEEL) && bullet.getUpgrade() == false) {
            bullet.setVisible(false);
        }
        if (block.getHealth() <1) {
            block.vis = false;
            SoundUtility.explosion2();
            explosions.add(new Explosion(block.x, block.y));
        }
    }

    /**
     * Check collision between tank and blocks
     *
     * @param r3 Rectangle
     * @return a boolean represents if there is a collision
     */
    public static boolean checkCollisionTankBlocks(Rectangle r3) {
        for (Block block : blocks) {
            Rectangle r2 = block.getBounds();
            BlockType type = BlockType.getTypeFromInt(block.getType());
            if (type.equals(BlockType.TREE)) {
            } else if (r3.intersects(r2)) {
                return true;
            }

        }
        return false;
    }

    /**
     * Check collision between bullets and blocks
     *
     * @param bullets array list for bullets
     * @param blocks  array list for blocks
     */
    public static void checkCollisionBulletsBlocks(ArrayList<Bullet> bullets,
                                                   ArrayList<Block> blocks) {

        for (int x = 0; x < bullets.size(); x++) {
            Bullet b = bullets.get(x);
            Rectangle r1 = b.getBounds();

            for (int i = 0; i < blocks.size(); i++) {
                Block aBlock = blocks.get(i);
                Rectangle r2 = aBlock.getBounds();

                if (r1.intersects(r2)) {
                    SoundUtility.BulletHitBrick();
                    CollisionBulletsBlocksHelper(b, aBlock);
                }
            }
        }
    }

    /**
     * Check collision between bullets and the player tank
     *
     * @param bullets array list for bullets
     * @param tank
     */
    public static void checkCollisionBulletsTank(ArrayList<Bullet> bullets,
                                                 Tank tank) {
        Rectangle r2 = tank.getBounds();
        for (int x = 0; x < bullets.size(); x++) {
            Bullet b = bullets.get(x);
            Rectangle r1 = b.getBounds();
            if (r1.intersects(r2) && b.isEnemy == true) {
                SoundUtility.BulletHitTank();
                b.vis = false;

                tank.downHealth(b.getDamage()   );
                if (tank.getHealth() < 1) {
                    SoundUtility.explosion1();
                    explosions.add(new ExplodingTank(tank.x, tank.y));
                    resetTankPosition(tank,true);
                }
            }
        }
    }

    /**
     * Check collision between bullets and enemy tanks
     *
     * @param bullets array list for bullets
     * @param TankAIs array list for Tank AIs
     */
    public static void checkCollisionBulletsTankAI(ArrayList<Bullet> bullets,
                                                   ArrayList<TankAI> TankAIs) {
        for (int x = 0; x < bullets.size(); x++) {
            Bullet b = bullets.get(x);
            Rectangle r1 = b.getBounds();

            for (int i = 0; i < TankAIs.size(); i++) {
                TankAI tankAI = TankAIs.get(i);
                Rectangle r2 = tankAI.getBounds();

                if (r1.intersects(r2) && b.isEnemy == false) {
                    tankAI.decreaseHP(b.getDamage());
                    b.vis = false;
                    SoundUtility.BulletHitTank();
                    if (tankAI.getHealth() < 1) {
                        incrementNum(tankAI);
                        if (tankAI.hasPowerUp()) {
                            powerUpX = tankAI.getX();
                            powerUpY = tankAI.getY();
                        }
                        tankAI.vis = false;
                        gameBoard.inCreaseNumEnemiesDestroyed();
                        SoundUtility.explosion1();
                        explosions.add(new ExplodingTank(tankAI.x, tankAI.y));
                    }
                }
            }
        }
    }

    /**
     * Increment number of the tankAI being destroyed
     *
     * @param tankAI a given tankAI
     */
    public static void incrementNum(TankAI tankAI) {
        String type = tankAI.getType();
        switch (type) {
            case "basic":
                enemyTankNum[0] += 1;
                break;
            case "fast":
                enemyTankNum[1] += 1;
                break;
            case "power":
                enemyTankNum[2] += 1;
                break;
            case "armor":
                enemyTankNum[3] += 1;
                break;
            default:
                break;
        }
    }

    /**
     * Get the array that stores the number of each enemy tank being destroyed
     *
     * @return enemyTankNum the array that stores the number of each enemy tank
     * being destroyed
     */
    public static int[] getEnemyTankNum() {
        return enemyTankNum;
    }

    /**
     * Reset the position of the tank
     *
     * @param atank
     */
    public static void resetTankPosition(Tank atank,boolean isdead) {
        atank.x = Map.mapInitPlayerX.get(new Random().nextInt(Map.mapInitPlayerX.size())) * 16;
        atank.y = Map.mapInitPlayerY * 16;
        if(isdead) {
            atank.resetHealth();
            atank.downLives();
            atank.starLevel = 0;
        }
    }

    /**
     * Check collision between the player and enemy tanks
     *
     * @param TankAIs array list for Tank AIs
     * @param atank   the player tank
     */
    public static void checkCollisionTankTankAI(ArrayList<TankAI> TankAIs,
                                                Tank atank) {
        Rectangle r1 = atank.getBounds();
        for (int i = 0; i < TankAIs.size(); i++) {
            TankAI tankAI = TankAIs.get(i);
            Rectangle r2 = tankAI.getBounds();
            if (r1.intersects(r2)) {
                explosions.add(new ExplodingTank(atank.x, atank.y));
                resetTankPosition(atank,true);
            }
        }
    }


}
