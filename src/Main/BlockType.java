package Main;

/**
 * Enum class for block on the map
 *
 */
public enum BlockType {

    /**
     * Enum types for blocks on the map
     */
    BLANK(0), BRICK(1), STEEL(2), RIVER(3), TREE(4), EDGE(5),  SHIELD(6), TANK(7), STAR(8), BOMB(9);
    // Integer that represents the value of each enum type
    private final int value;

    /**
     * Constructor of the block type
     *
     * @param value integer that represents the value of each enum type
     */
    private BlockType(int value) {
        this.value = value;
    }

    /**
     * Return integer values that correspond each enum type
     *
     * @return an integer value that corresponds each enum type
     */
    public int getValue() {
        return value;
    }

    /**
     * Return block type given integer value
     *
     * @param value an integer value that corresponds each enum type
     * @return BlockType in the game
     */
    public static BlockType getTypeFromInt(int value) {
        return BlockType.values()[value];
    }

}
