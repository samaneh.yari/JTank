package Items;

import java.io.Serializable;

/**
 * Block is extended from the Item class. It adds health and type to the
 * Items allowing for different types of blocks and ways to manipulate block
 * health
 */
public class Block extends Item implements Serializable {
    public int health = 1;
    private int type;

    /**
     * onstructor for the Block
     * @param  x represents the starting x location in pixels
     * @param  y represents the starting y location in pixels
     */
    public Block(int x, int y) {
        super(x, y);
    }

    public void lowerHealth(int damage) {
        health -= damage;
    }

    public int currentHealth() {
        return health;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void updateAnimation() {

    }

}
