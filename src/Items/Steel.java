package Items;

import java.io.Serializable;

/**
 * Steel is a Block with type 2 and health 1
 *
 */
public class Steel extends Block  implements Serializable {

    public Steel(int x, int y) {
        super(x, y);
        loadImage("image/wall_steel.png");
        setHealth(1);
        setType(2);
    }

}
