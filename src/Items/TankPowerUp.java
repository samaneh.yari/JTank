
package Items;

import java.io.Serializable;

/**
 * TankPowerUp extends the PowerUp and sets the type as 7
 *
 */
public class TankPowerUp extends PowerUp implements Serializable {
    public TankPowerUp(int x, int y) {
        super(x, y);
        loadImage("image/powerup_tank.png");
        setType(7);
        s = "image/powerup_tank.png";
    }

}
