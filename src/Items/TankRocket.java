package Items;

import Main.Map;

import java.io.Serializable;

/**
 * TankRocket is a Item. Rockets can move with a predefined speed.
 * Additionally.
 * TankBullet can be spawned either by players tank
 */
public class TankRocket extends TankBullet implements Serializable {
    private final int BOARD_WIDTH = Map.mapWidth;
    private final int BOARD_HEIGHT = Map.mapHeight;
    private int angle;

    /**
     * @param x represents the starting x location in pixels
     * @param y represents the starting y location in pixels
     */
    public TankRocket(int x, int y, int angle,int damage) {
        super(x, y, angle,damage);
        this.angle = angle;
        if (angle >= 355 || angle <= 5)
            loadImage("image/RocketFire_0.png");
        else if (angle > 5 && angle < 40)
            loadImage("image/RocketFire_30.png");
        else if (angle >= 40 && angle <= 50)
            loadImage("image/RocketFire_45.png");
        else if (angle > 50 && angle < 85)
            loadImage("image/RocketFire_60.png");
        else if (angle >= 85 && angle <= 95)
            loadImage("image/RocketFire_90.png");
        else if (angle > 95 && angle < 130)
            loadImage("image/RocketFire_120.png");
        else if (angle >= 130 && angle <= 140)
            loadImage("image/RocketFire_135.png");
        else if (angle > 140 && angle < 175)
            loadImage("image/RocketFire_150.png");
        else if (angle >= 175 && angle <= 185)
            loadImage("image/RocketFire_180.png");
        else if (angle > 185 && angle < 220)
            loadImage("image/RocketFire_210.png");
        else if (angle >= 220 && angle <= 230)
            loadImage("image/RocketFire_225.png");
        else if (angle > 230 && angle < 265)
            loadImage("image/RocketFire_240.png");
        else if (angle >= 265 && angle <= 275)
            loadImage("image/RocketFire_270.png");
        else if (angle > 275 && angle < 310)
            loadImage("image/RocketFire_300.png");
        else if (angle >= 310 && angle <=320)
            loadImage("image/RocketFire_315.png");
        else if (angle > 320 && angle < 355)
            loadImage("image/RocketFire_330.png");
        BULLET_SPEED=6;
    }

}
