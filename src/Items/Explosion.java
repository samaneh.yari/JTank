package Items;

import java.io.Serializable;

/**
 * Explosion extends Animation. This is the explosion used for bullets and
 * various types of blocks.
 *
 */
public class Explosion extends Animation implements Serializable {

    public Explosion(int x, int y) {
        super(x - 8, y - 8);
        loadImage("image/bullet_explosion_1.png");
    }

    @Override
    public void updateAnimation() {
        if ((System.currentTimeMillis() - initialTime) > 125) {
            loadImage("image/bullet_explosion_2.png");
        }
        if ((System.currentTimeMillis() - initialTime > 250)) {
            loadImage("image/bullet_explosion_3.png");
        }
        if ((System.currentTimeMillis() - initialTime > 300)) {
            super.vis = false;
        }

    }

}
