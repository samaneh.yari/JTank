package Items;
/**
 * This Item classes is the father of all Items can be showen in map
 */

import java.awt.Image;
import java.awt.Rectangle;
import java.io.Serializable;
import javax.swing.ImageIcon;

public class Item implements Serializable {
    public int x;
    public int y;
    public int width;
    public int height;
    public boolean vis;
    public String imageName;

    public Item(int x, int y) {

        this.x = x;
        this.y = y;
        vis = true;
    }

    public Item(int x, int y, String imageName) {
        this.x = x;
        this.y = y;
        vis = true;
        loadImage(imageName);
    }

    protected void loadImage(String imageName) {
        this.imageName = imageName;
        ImageIcon i = new ImageIcon(imageName);
        Image image = i.getImage();
        width = image.getWidth(null);
        height = image.getHeight(null);
    }

    public Image getImage() {
        return new ImageIcon(imageName).getImage();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isVisible() {
        return vis;
    }

    public void setVisible(Boolean visible) {
        vis = visible;
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, width, height);
    }



    @Override
    public String toString() {
        return x + "|" + y + "|" + PictureID.getID(imageName);
    }
}
