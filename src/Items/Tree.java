package Items;

import java.io.Serializable;

/**
 * Tree is a block with type 5 and health 1
 *
 */
public class Tree extends Block implements Serializable {
    public Tree(int x, int y) {
        super(x, y);
        loadImage("image/trees.png");
        setType(4);
        setHealth(1);
    }

}
