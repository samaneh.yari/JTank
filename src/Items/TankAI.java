package Items;

import Main.*;

import java.awt.Image;
import java.awt.Rectangle;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 * TankAI extends Item. TankAI represents the enemy in the game. TankAI has an
 * array of bullets and is capable of moving and firing bullets depending on
 * difficulties. TankAI has 4 different types that is upgraded in several ways.
 * TankAI can also give player power up.
 */
public class TankAI extends Item  implements Serializable {

    private final int BOARD_WIDTH = Map.mapWidth;
    private final int BOARD_HEIGHT = Map.mapHeight;
    private ArrayList<Bullet> bullets;
    private boolean powerUp;
    private int dx, dy;
    public int direction;
    private int health;
    private String difficulty;
    private String type;
    private int dirTimer = 0;
    private int dirUpdateInterval;
    private int fireTimer = 0;
    private int fireUpdateInterval;
    private double speedConst;
    public int damage;
    public int initHealth;


    /**
     * @param x          represents the starting x location in pixels
     * @param y          represents the starting y location in pixels
     * @param difficulty represents the difficulty
     * @param type       represents the type of the AI
     * @param powerUp    indicates if the AI will give player powerUp or not
     */
    public TankAI(int x, int y, String difficulty, String type, boolean powerUp) {
        super(x, y);
        bullets = new ArrayList<>();
        direction = 0;
        this.vis = true;
        this.powerUp = powerUp;
        this.difficulty = difficulty;
        this.type = type;
        this.setUp();
        this.imageSetUp();
    }

    /**
     * setUp() is a helper function. It will handle setting up health, speed and
     * fire rate for the AI, depending on the type of the AI.
     */
    private void setUp() {
        if ("basic".equals(this.type)) {
            this.health = 2;
            this.speedConst = 1;
            if (difficulty.equals("easy")) {
                damage = 1;
                dirUpdateInterval = 30;
                fireUpdateInterval = 80;
            } else if (difficulty.equals("normal")) {
                damage = 1;
                dirUpdateInterval = 30;
                fireUpdateInterval = 75;
            } else if (difficulty.equals("hard")) {
                damage = 3;
                dirUpdateInterval = 30;
                fireUpdateInterval = 70;
            }
        } else if ("armor".equals(this.type)) {
            this.health = 8;
            this.speedConst = 1;
            if (difficulty.equals("easy")) {
                damage = 2;
                dirUpdateInterval = 30;
                fireUpdateInterval = 80;
            } else if (difficulty.equals("normal")) {
                damage = 2;
                dirUpdateInterval = 30;
                fireUpdateInterval = 75;
            } else if (difficulty.equals("hard")) {
                damage = 4;
                dirUpdateInterval = 30;
                fireUpdateInterval = 70;
            }
        } else if ("power".equals(this.type)) {
            this.health = 6;
            this.speedConst = 1;
            if (difficulty.equals("easy")) {
                damage = 1;
                dirUpdateInterval = 30;
                fireUpdateInterval = 40;
            } else if (difficulty.equals("normal")) {
                damage = 2;
                dirUpdateInterval = 30;
                fireUpdateInterval = 35;
            } else if (difficulty.equals("hard")) {
                damage = 2;
                dirUpdateInterval = 30;
                fireUpdateInterval = 30;
            }
        } else if ("fast".equals(this.type)) {
            this.health = 6;
            this.speedConst = 2;
            if (difficulty.equals("easy")) {
                damage = 1;
                dirUpdateInterval = 30;
                fireUpdateInterval = 80;
            } else if (difficulty.equals("normal")) {
                damage = 1;
                dirUpdateInterval = 30;
                fireUpdateInterval = 75;
            } else if (difficulty.equals("hard")) {
                damage = 2;
                dirUpdateInterval = 30;
                fireUpdateInterval = 70;
            }
        }
        initHealth = this.health;
    }

    /**
     * imageSetUp() is a helper function. It will handle setting up the image
     * for different direction depending on the type of the AI.
     */
    private void imageSetUp() {
        dirUpdate();
    }

    public String getDifficulty() {
        return difficulty;
    }

    public ArrayList<Bullet> getBullets() {
        return bullets;
    }

    public boolean hasPowerUp() {
        return powerUp;
    }

    public void decreaseHP(int damage) {
        this.health -= damage;
    }

    public int getHealth() {
        return health;
    }

    /**
     * actionEasy() handles action of AI with easy difficulty.
     */
    public void actionEasy() {
        if (this.dirTimer >= this.dirUpdateInterval) {
            randomDir();
            this.dirTimer = 0;
        } else {
            this.dirTimer++;
        }
        this.move();
        if (this.fireTimer >= this.fireUpdateInterval) {
            this.fire();
            this.fireTimer = 0;
        } else {
            this.fireTimer++;
        }
    }

    /**
     * actionNormal() handles action of AI with normal difficulty.
     *
     * @param tank: the player tank
     */
    public void actionNormal(Tank tank) {
        Random randomDir = new Random();
        if (this.dirTimer >= this.dirUpdateInterval) {
            int random = randomDir.nextInt(20);
            if (random % 4 == 0) {
                randomDir();
            } else {
                toTankDir(tank);
            }
            this.dirTimer = 0;
        } else {
            this.dirTimer++;
        }
        this.move();
        Rectangle theTank = new Rectangle(x + dx, y + dy, width, height);

        if (CollisionUtility.checkCollisionTankBlocks(theTank) == true) {
            if (randomDir.nextBoolean() && this.fireTimer < 3) {
                this.fire();
                this.fireTimer++;
            }
        }
        if (this.fireTimer >= this.fireUpdateInterval) {
            this.fire();
            this.fireTimer = 0;
        } else {
            this.fireTimer++;
        }
    }

    /**
     * actionHard() handles action of AI with hard difficulty.
     *
     * @param tank: the player tank
     */
    public void actionHard(Tank tank) {
        Random randomDir = new Random();
        if (this.dirTimer >= this.dirUpdateInterval) {
            int random = randomDir.nextInt(7);
            if (random % 6 == 1) {
                randomDir();
            } else {
                toTankDir(tank);
            }
            this.dirTimer = 0;
        } else {
            this.dirTimer++;
        }
        Rectangle theTank = new Rectangle(x + dx, y + dy, width, height);
        this.move();
        if (CollisionUtility.checkCollisionTankBlocks(theTank) == true) {
            if (randomDir.nextBoolean() && this.fireTimer < 3) {
                this.fire();
                this.fireTimer++;
            }
        }
        if (this.fireTimer >= this.fireUpdateInterval) {
            this.fire();
            this.fireTimer = 0;
        } else {
            this.fireTimer++;
        }
    }

    /**
     * move() handles moving of the AI.
     */
    public void move() {
        if(type.equals("power"))
            return;
        Rectangle theTank = new Rectangle(x + dx, y + dy, width, height);

        if (CollisionUtility.checkCollisionTankBlocks(theTank) == false) {
            x += dx;
            y += dy;
        }

        if (x > BOARD_WIDTH - width) {
            x = BOARD_WIDTH - width;
        }

        if (y > BOARD_HEIGHT - height) {
            y = BOARD_HEIGHT - height;
        }
        if (x < 1) {
            x = 1;
        }

        if (y < 1) {
            y = 1;
        }
    }

    /**
     * fire() handle firing bullet for the AI.
     */
    public void fire() {
        Bullet aBullet;
        switch (direction) {
            case 0:
                aBullet = new Bullet(x + width / 3, y, 0, true, damage);
                break;
            case 1:
                aBullet = new Bullet(x + width, y + height / 3, 1, true, damage);
                break;
            case 2:
                aBullet = new Bullet(x + width / 3, y + height, 2, true, damage);
                break;
            default:
                aBullet = new Bullet(x, y + height / 3, 3, true, damage);
                break;
        }
        bullets.add(aBullet);

    }

    /**
     * randomDir() handle finding the direction randomly.
     */
    public void randomDir() {
        Random randomDir = new Random();
        this.direction = randomDir.nextInt(4);
        dirUpdate();
    }

    /**
     * toTankDir() handle finding direction to the player's tank.
     *
     * @param tank: the player tank.
     */
    public void toTankDir(Tank tank) {
        int tankX = tank.getX();
        int tankY = tank.getY();
        Random randomDir = new Random();
        if (randomDir.nextBoolean()) {
            if (this.getY() > tankY) {
                this.direction = 0;
            } else {
                this.direction = 2;
            }
        } else if (this.getX() > tankX) {
            this.direction = 3;
        } else if (this.getX() < tankX) {
            this.direction = 1;
        } else {
            this.direction = 3;
        }
        dirUpdate();
    }

    /**
     * dirUpdate() handles updating image and acceleration of the AI according
     * to direction
     */
    private void dirUpdate() {
        ImageIcon ii;

        String picType = "image/" + getPicType() + ".png";
        loadImage(picType);
        switch (this.direction) {
            case 0:
                this.dx = (int) (0 * this.speedConst);
                this.dy = (int) (-1 * this.speedConst);
                break;
            case 1:
                this.dx = (int) (1 * this.speedConst);
                this.dy = (int) (0 * this.speedConst);
                break;
            case 2:
                this.dx = (int) (0 * this.speedConst);
                this.dy = (int) (1 * this.speedConst);
                break;
            case 3:
                this.dx = (int) (-1 * this.speedConst);
                this.dy = (int) (0 * this.speedConst);
                break;
        }
    }

    private String getPicType() {
        String str = "tank_";
        str += type + "_";

        switch (this.direction) {
            case 0:
                str += "up";
                break;
            case 1:
                str += "right";
                break;
            case 2:
                str += "down";
                break;
            case 3:
                str += "left";
                break;
        }
        if (type.equals("armor"))
            if (health > initHealth / 2)
                str += "_C2";
            else if (health > initHealth / 3)
                str += "_C1";
            else
                str += "_C0";
        if (powerUp)
            str += "_f";
        return str;
    }

    /**
     * Get the type of the Enemy tank
     *
     * @return type
     */
    public String getType() {
        return type;
    }
}
