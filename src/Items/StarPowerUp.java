
package Items;

import java.io.Serializable;

/**
 * StarPowerUp extends PowerUp and sets the type as 8
 *
 */
public class StarPowerUp extends PowerUp implements Serializable {
    public StarPowerUp(int x, int y) {
        super(x, y);
        loadImage("image/powerup_star.png");
        setType(8);
        s = "image/powerup_star.png";
    }

}
