package Items;

import java.io.Serializable;

/**
 * TankSpawn is an animation used when enemy tanks are spawned
 */
public class TankSpawn extends Animation implements Serializable {
    /**
     * @param  x represents the starting x location in pixels
     * @param  y represents the starting y location in pixels
     */
    public TankSpawn(int x, int y) {
        super(x, y);
        loadImage("image/appear_1.png");
    }

    @Override
    public void updateAnimation() {
        if ((System.currentTimeMillis() - initialTime) > 100) {
            loadImage("image/appear_2.png");
        }
        if ((System.currentTimeMillis() - initialTime > 200)) {
            loadImage("image/appear_3.png");
        }
        if ((System.currentTimeMillis() - initialTime > 300)) {
            loadImage("image/appear_4.png");
        }
        if ((System.currentTimeMillis() - initialTime > 400)) {
            loadImage("image/appear_1.png");
        }
        if ((System.currentTimeMillis() - initialTime > 500)) {
            loadImage("image/appear_2.png");
        }
        if ((System.currentTimeMillis() - initialTime > 600)) {
            loadImage("image/appear_3.png");
        }
        if ((System.currentTimeMillis() - initialTime > 700)) {
            loadImage("image/appear_4.png");
        }
        if ((System.currentTimeMillis() - initialTime > 800)) {
            super.vis = false;
        }
    }
}
