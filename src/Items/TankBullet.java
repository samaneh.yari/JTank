package Items;

import Main.Map;

import java.io.Serializable;

/**
 * TankBullet is a Item. Bullets can move with a predefined speed.
 * Additionally, bullets can be upgraded to destroy steel bricks.
 * TankBullet can be spawned either by players tank
 */
public class TankBullet extends Bullet implements Serializable {
    protected int BULLET_SPEED = 8;
    private final int BOARD_WIDTH = Map.mapWidth;
    private final int BOARD_HEIGHT = Map.mapHeight;
    private int angle;

    /**
     * @param x represents the starting x location in pixels
     * @param y represents the starting y location in pixels
     */
    public TankBullet(int x, int y, int angle,int damage) {
        super(x, y, 0, false,damage);

        this.angle = angle;
        loadImage("image/TankBullet.png");
    }

    @Override
    public void move() {
        x += BULLET_SPEED*Math.cos(Math.toRadians(angle));
        y -= BULLET_SPEED*Math.sin(Math.toRadians(angle));
        if (x > BOARD_WIDTH + 100 + width) {
            vis = false;
        }
        if (x < -width - 100) {
            vis = false;
        }
        if (y > BOARD_HEIGHT + height + 100) {
            vis = false;
        }
        if (y < -height - 100) {
            vis = false;
        }
    }

}
