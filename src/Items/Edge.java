package Items;

import java.io.Serializable;

/**
 * Edge class
 */
public class Edge extends Block  implements Serializable {
    public Edge(int x, int y) {
        super(x, y);
        loadImage("image/edge.png");
        setType(5);
        setHealth(1);
    }
}
