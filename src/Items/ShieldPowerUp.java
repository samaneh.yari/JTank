package Items;

import java.io.Serializable;

/**
 * ShieldPowerUp extends PowerUp and sets the type at 12
 *
 */
public class ShieldPowerUp extends PowerUp implements Serializable {
    public ShieldPowerUp(int x, int y) {
        super(x, y);
        loadImage("image/powerup_helmet.png");
        setType(6);
        s = "image/powerup_helmet.png";
    }

}
