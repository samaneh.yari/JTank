
package Items;

import java.io.Serializable;

/**
 * BombPowerUp extends PowerUp
 */
public class BombPowerUp extends PowerUp implements Serializable {
    public BombPowerUp(int x, int y) {
        super(x, y);
        loadImage("image/powerup_grenade.png");
        setType(9);
        s = "image/powerup_grenade.png";
    }

}
