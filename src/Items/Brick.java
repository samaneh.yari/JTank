package Items;

import java.io.Serializable;

/**
 * Brick is a block with type 1 and health 1.
 */
public class Brick extends Block implements Serializable {
    public Brick(int x, int y) {
        super(x, y);
        loadImage("image/wall_brick.png");
        setType(1);
        setHealth(10);
    }

}
