package Items;

import Main.*;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.*;

/**
 * Tank extends Item The Tank represents the player in the game. The tank has
 * an array of bullets and is capable of moving and firing bullets depending on
 * key events (arrow keys for movement and clink for firing bullets) in four
 * different directions. The tank also can be upgraded in several ways
 * increasing the firing speed movement speed and the ability to break steel
 * blocks
 */
public class Tank extends Item implements Serializable {

    private int dx;
    private int dy;
    public int direction;
    private ArrayList<Bullet> bullets;
    private long lastFired = 0;
    private int health;
    public int starLevel = 0;
    public int lives;
    public boolean fireMode;
    public static final int MAX_HEALTH = 10;
    public static final int MAX_LIVE = 3;
    private Timer gunFire;
    private MouseEvent aim;
    public int rocketCount = 50;
    public int bulletCount = 200;
    public boolean player1;
    public int gunX;
    public int gunY;

    public void upLives() {
        this.lives += 1;
    }


    public int getRocketCount() {
        return rocketCount;
    }

    public int getBulletCount() {
        return bulletCount;
    }

    public void downLives() {
        this.lives -= 1;
    }

    public int getHealth() {
        return health;
    }

    public void upStarLevel() {
        starLevel += 1;
    }

    public int getLives() {
        return this.lives;
    }

    public void upHealth() {
        this.health += 1;
    }

    public void addAmmo() {
        bulletCount += 50;
        rocketCount += 10;
    }

    public boolean isMaxLives() {
        return lives == MAX_LIVE;
    }

    public boolean isMaxHealth() {
        return health == MAX_HEALTH;
    }

    public void downHealth(int damage) {
        this.health -= damage;
    }

    public void resetHealth() {
        this.health = MAX_HEALTH;
    }

    /*
     * @param  x represents the starting x location in pixels
     * @param  y represents the starting y location in pixels
     * @param  lives is the lives which the tank posses
     */
    public Tank(int x, int y, boolean player1) {
        super(x, y);
        this.player1 = player1;
        if (player1)
            loadImage("image/tank_player1_up_s0.png");
        else
            loadImage("image/tank_player2_up_s0.png");
        this.bullets = new ArrayList<>();
        this.direction = 0;
        this.lives = 1;
        this.fireMode = false;
        lives = MAX_LIVE;
        health = MAX_HEALTH;
        makeGunFireTimer();
    }

    /*
     * @param  x represents the starting x location in pixels
     * @param  y represents the starting y location in pixels
     * @param  picName is the path of Image
     */
    public Tank(int x, int y, String picName) {
        super(x, y);
        loadImage(picName);
    }

    public void makeGunFireTimer() {
        gunFire = new Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int time;
                if (!fireMode)
                    time = 500;
                else
                    time = 100;
                if ((System.currentTimeMillis() - lastFired) > time) {
                    fire(getAngle(x + (width / 2), y + (height / 2), aim.getX(), aim.getY()));
                    lastFired = System.currentTimeMillis();
                }
            }
        });
    }

    public void move() {
        Rectangle theTank = new Rectangle(x + dx, y + dy, width, height);
        if (CollisionUtility.checkCollisionTankBlocks(theTank) == false) {
            x += dx;
            y += dy;
        }

        if (x > Map.mapWidth - width) {
            x = Map.mapWidth - width;
        }

        if (y > Map.mapHeight - height) {
            y = Map.mapHeight - height;
        }
        if (x < 1) {
            x = 1;
        }

        if (y < 1) {
            y = 1;
        }
    }


    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A) {
            dx = -1;
            dy = 0;
            if (starLevel > 1) {
                dx = -2;
            }
            direction = 3;
            String picType = "image/" + getPicType() + ".png";
            loadImage(picType);
        } else if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) {
            dx = 1;
            dy = 0;
            if (starLevel > 1) {
                dx = 2;
            }
            direction = 1;
            String picType = "image/" + getPicType() + ".png";
            loadImage(picType);
        } else if (key == KeyEvent.VK_UP || key == KeyEvent.VK_W) {
            dy = -1;
            dx = 0;
            if (starLevel > 1) {
                dy = -2;
            }
            direction = 0;
            String picType = "image/" + getPicType() + ".png";
            loadImage(picType);
        } else if (key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S) {
            dy = 1;
            dx = 0;
            if (starLevel > 1) {
                dy = 2;
            }
            direction = 2;
            String picType = "image/" + getPicType() + ".png";
            loadImage(picType);
        }
    }

    public void keyReleased(KeyEvent e) {

        int key = e.getKeyCode();
        if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_A) {
            dx = 0;
        }

        if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_D) {
            dx = 0;
        }

        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_W) {
            dy = 0;
        }

        if (key == KeyEvent.VK_DOWN || key == KeyEvent.VK_S) {
            dy = 0;
        }
    }


    public int getStarLevel() {
        return starLevel;
    }

    private int getAngle(int srcX, int srcY, int disX, int disY) {
        float angle = (float) Math.toDegrees(Math.atan2((-disY) - (-srcY), disX - srcX));
        if (angle < 0)
            angle += 360;
        return (int) angle;
    }


    public void fire(int angle) {
        Bullet aBullet = null;
        if (fireMode) {
            if (bulletCount > 0) {
                bulletCount--;
                aBullet = new TankBullet(x + (width / 2), y + (height / 2), angle, starLevel + 1);
            }
        } else {
            if (rocketCount > 0) {
                rocketCount--;
                aBullet = new TankRocket(x + (width / 2), y + (height / 2), angle, (starLevel + 1) * 3);
            }
        }
        if (aBullet != null) {
            SoundUtility.fireSound();
            bullets.add(aBullet);
        }
    }

    public void startFireing() {
        gunFire.start();
    }

    public void stopFireing() {
        gunFire.stop();
    }


    public void mousePressed(MouseEvent e) {
        if (e.getButton() == 1) {
            aim = e;
            startFireing();
        }
    }

    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == 1) {
            stopFireing();
        }
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3)
            fireMode = !fireMode;
    }

    public ArrayList<Bullet> getBullets() {
        return bullets;
    }

    public void mouseMoved(MouseEvent e) {
        aim = e;
        syncGun();
    }

    public void syncGun() {
        if (aim != null) {
            int angle = getAngle(x + (width / 2), y + (height / 2), aim.getX(), aim.getY());
            gunX = (int) (x + (width / 2) + 20 * Math.cos(Math.toRadians(angle)));
            gunY = (int) (y + (height / 2) - 20 * Math.sin(Math.toRadians(angle)));
        }
    }

    public int getGunX() {
        return gunX;
    }

    public int getGunY() {
        return gunY;
    }

    private String getPicType() {
        String str = "tank_";
        if (player1)
            str += "player1_";
        else
            str += "player2_";
        switch (this.direction) {
            case 0:
                str += "up";
                break;
            case 1:
                str += "right";
                break;
            case 2:
                str += "down";
                break;
            case 3:
                str += "left";
                break;
        }
        switch (this.starLevel) {
            case 0:
                str += "_s0";
                break;
            case 1:
                str += "_s1";
                break;
            case 2:
                str += "_s2";
                break;
            default:
                str += "_s3";
                break;
        }
        return str;
    }
}
